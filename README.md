# Virtual Development Environment Setup

## Terminology:
* "host machine" - this is your mac
* "catalonia" - this is the alias for the virtual ubuntu box we are creating

## Directory structure:
* place all of your projects in the ./code directory. This will become a shared volume, available on `catalonia`

## Workflow:
* do any type of version control (git, ide work, etc) from your host machine
* do any type of code execution (mix, elixir, node, yarn, etc) on `catalonia`

## Dependencies:
* Install XCode Command Line Tools
  * From your host machine, run: `xcode-select --install`
* [Vagrant 2.2.5](https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_x86_64.dmg)
* [VirtualBox 6.0.12](https://download.virtualbox.org/virtualbox/6.0.12/VirtualBox-6.0.12-133076-OSX.dmg)
  * After MacOS 10.14.5, you might need to do this http://osxdaily.com/2018/12/31/install-run-virtualbox-macos-install-kernel-fails/
* After installing vagrant, run `./install-vagrant-plugins.sh`

## Lets fuckin' send it:
* install all the stuff as listed where [dependencies](dependencies)
* run `vagrant up`
  * you will be prompted for your password, make sure to enter that
* after its up, `run vagrant ssh catalonia` to get into the box
* code lives in ~/code (synced volume)
* your box is accessible at catalonia.compost-connect.com
  * ie: catalonia.compost-connect.com:4000 would be the default phoenix homepage
* when youre done, run `vagrant halt` to stop the virtual machine

## Troubleshooting
* to destroy your vagrant machine:
  * `vagrant destroy --force`
