#  set the password for postgres user (by default no password)
sudo -u postgres psql -c"ALTER user postgres WITH PASSWORD 'postgres'"
#  create role and db for vagrant so we can easily use psql
sudo -u postgres createuser -s vagrant
sudo -u postgres createdb vagrant
