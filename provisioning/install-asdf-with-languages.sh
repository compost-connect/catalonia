#  install asdf
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.4
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bashrc
echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc

# source asdf into our current path
. $HOME/.asdf/asdf.sh

# install erlang, elixir plugins
asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git

# install nodejs plugin
asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs.git
bash ~/.asdf/plugins/nodejs/bin/import-release-team-keyring

# install default versions of erlang, elixir, and nodejs
export KERL_CONFIGURE_OPTIONS="--disable-debug --without-javac"
asdf install erlang 21.0.9
asdf global erlang 21.0.9
asdf install elixir 1.7.4
asdf global elixir 1.7.4
asdf install nodejs 8.15.1
asdf global nodejs 8.15.1
