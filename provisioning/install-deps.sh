# install general deps for erlang, js, etc
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt-get update
# erlang deps
apt-get -y install build-essential autoconf m4 libncurses5-dev libwxgtk3.0-dev libgl1-mesa-dev libglu1-mesa-dev libpng-dev libssh-dev unixodbc-dev xsltproc fop
# yarn
apt-get install -y --no-install-recommends yarn
# postgres
apt-get install -y postgresql postgresql-contrib
# inotify for phoenix file watcher
apt-get install -y inotify-tools
# time syncronization tool
apt-get install -y ntpdate
