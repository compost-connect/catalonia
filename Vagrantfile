BOX_IMAGE = "bento/ubuntu-16.04"
BOX_VERSION = "201708.22.0"
DEV_BOX_NAME = "catalonia"

Vagrant.require_version ">= 1.9.2"

# find all non-hidden directories under ./code/
projects = Dir.entries(File.dirname(__FILE__) + "/code")
  .select {|d| not d =~ /^\.+/}
  .map {|d| d.concat(".local.compost-connect.com").gsub("_", "-")}

Vagrant.configure(2) do |config|

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true

  config.vm.define DEV_BOX_NAME do |dev|
    dev.vm.box = BOX_IMAGE
    dev.vm.box_version = BOX_VERSION
    dev.vm.box_check_update = true

    dev.vm.hostname = DEV_BOX_NAME
    dev.vm.network "private_network", ip: "192.168.150.150"
    dev.hostmanager.aliases =
      projects.concat ["#{DEV_BOX_NAME}.compost-connect.com"]

    #mount our host directory by means of NFS instead of default virtualbox
    #sharing mechanism
    dev.vm.synced_folder "./code", "/home/vagrant/code", type: "nfs"
    dev.vm.synced_folder "./provisioning", "/home/vagrant/provisioning", type: "nfs"

    # add file watches so that we can run a buncha mix phx.server alongside test.watches
    dev.vm.provision "shell", path: "provisioning/install-deps.sh"
    dev.vm.provision "shell", path: "provisioning/install-asdf-with-languages.sh", privileged: false
    dev.vm.provision "shell", path: "provisioning/setup-postgres.sh"
    dev.vm.provision "shell", inline: "sudo sh -c \"echo fs.inotify.max_user_watches=524288 >> /etc/sysctl.conf\""

    dev.vm.provider :virtualbox do |vm|
      vm.memory = 4096
      vm.cpus = 4
      vm.customize ["modifyvm", :id, "--audio", "none"]
    end
  end

  config.vm.provider DEV_BOX_NAME do |vb|
    vb.customize [ "guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 1000 ]
  end
end
